The deal:
	- Thomas Slide show first
	- Page with this info and project that goes with it
	- Walk through the project
	- Q&A
	- Some longer term support
		- Converting python to python containers
		- Node.JS
		- HTML
		- Java
		- DB's
		- Templates?

Introduction to Docker
- What is Docker good for?
	- Repeat deployments (It worked on my machine)
	- Known good states (It was working yesterday)
		- Can either use an old, known-good, image or rebuild from a was-good dockerfile. Images are better.
	- Easy, isolated updates (Version 3.6 came out last week)
	- Scaling (When 10x the workers do 2x the work)
	- Building custom-off-the-shelf products (Why reinvent the wheel when you're not a wheel merchant?)
		- Docker Hub has just about everything 
	- Fire up, work, die, repeat
		- https://medium.com/devopsion/life-and-death-of-a-container-146dfc62f808
	- Isolation!

- Where did it come from
	- What issues was it built to address?
	- FOSS?
	- Linux Kernal? 

- Docker Terms
	- Image
		- Defined by Dockerfile
		- BaseImage
			- Root of Dockerfile
	- Repository
		- it's where images sleep
			- Docker Hub/Public Repo
			- Private Repo
			- Local Repo
	- Container
		- It's a running image, smallest unit of work in a "containerised" deployment. 
	- Build
		- The act of going from the dockerfile to the image
		x <Docker compose build>
	- Deployment
		- It's the action of applying a configuration to an image to create a create a container 
		x Docker-compose up
	- Push image to repo
		- It's the action of storing a built image in a safe location in order to use it later (either as is or as a base image)
	- Configuration
		- How does your container know what to do? 
		- Where to store data
		- How to talk to other containers
		- Secret configuration
	- Mounted Volume
		- Disk
		- Hardware
		- Some Linux things like devices and system datetime
	- Tag
		- The dreaded "latest" tag
	- Localhost 
		- This is a trap. When you're inside a container what is the localhost?
	
- What is a Dockerfile?
	- Base Image
	- Build arguments
	- Need to read up a little more on this. It's been a while
	- https://docs.docker.com/engine/reference/builder/
	
- What is a Docker-compose file
	- Is this needed? No, everything can be configured when using "docker run" BUT IT IS VERY USEFUL.
	- Configure SET of services, nobody actually uses a single container
	- Repeat configurations
	- Environment variables
		- the file vs the system
		- how to store this, 
		- how to share this (orchastration, rancher, kubernetes) 
	- Logging
		- why do you want to log?
		- when do you NOT want to log? 
		- elasticsearch and docker integration
	- Resource limits
		- real world
	- Mounted Volumes
		- persistance between container builds/runs
	- https://docs.docker.com/compose/compose-file/
	
- Some Docker gotcha's /FAQ's/Best Practices
	- My container died
		- Containers only run when they're doing something. Once finished they exit.
	- I can't ping/ssh/tcp into "localhost"
		- Localhost isn't who you think it is
	- My rebuild isn't working
		- Did you use the "latest" tag? Did you push the built image to a repository? Did some other weirdness happen (ubuntu legacy versions)?
	- Single container, single use
	- Where to store data, physically 
	- Minimal layers/ appropriate stacking
	- Docker networks
		- routing traffic between containers
			
- The Docker Starter Gitlab Project
	- Building a dockerfile
		- How to build a simple python 3 docker container
		- Pip Requirements
		- Run forever or run once and die?
		
	- Building a compose file
	
	- Deploying a project	
		- User groups
		
	- Maintaining a project
		- SSH
		- Deployment Keys
		- Checking logs
		- Checking if containers are running
		- Where are the volumes mounting
		- Exec'ing into a container
			- Docker ps
			- docker-compose ps
			- docker exec
			- docker-compose logs -f --tail 100
			- docker prune (cleaning images and volumes, no-cache etc) 
			- cleaning docker logs
			- docker-compose down/up/stop
			- docker info
			- 
		
- Next steps...
	- Container Orchastration
	- Multi machine management
	- Cloud deployment (same thing) 
	- Continous deployment/integration
	- Build pipelines/auto deployment/Jenkins
	- Log monitoring
	- Auto rollback/backups/versioning
	- Safe configuration storage and image storage