#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 24 15:05:19 2017

@author: rory
"""
import sys
import argparse
import logging 
import os
import json
import time

log = logging.getLogger('main')
# log.setLevel('DEBUG')

def do_work():  
    '''
    Post processing from message broker.
    '''
    log.info('Getting ready to do work...')
    time.sleep(30)
    
    orca = "orca.dhcp.meraka.csir.co.za" 
    prod_db = "prod_db"
    dev_db = "dev_db"
    google = "www.google.com"
    
    while True:
        
        log.info('Trying to ping google')
        response = os.system("ping -c 1 " + google)
        log.info('Google Ping: ' + str(response))
        
        log.info('Trying to ping Orca')
        response = os.system("ping -c 1 " + orca)
        log.info('Orca Ping: ' + str(response))
        
        log.info('Trying to ping prod_db')
        response = os.system("ping -c 1 " + prod_db)
        log.info('Prod DB Ping: ' + str(response))
        
        log.info('Trying to ping dev_db')
        response = os.system("ping -c 1 " + dev_db)
        log.info('Dev DB Ping: ' + str(response))
        time.sleep(30)
        
    log.info('Worker shutdown...')


def main(args):
    '''
    Setup logging, read config, fire up the Rabbit and Database wrappers and
    then process some messages. Messages come from text file or from web server
    '''
    logging.basicConfig(
        stream=sys.stdout,
        format='%(asctime)s - %(levelname)s - %(name)s - %(message)s',
        #level= log.debug)
        level=getattr(logging, args.loglevel))

    log.setLevel(getattr(logging, args.loglevel)) 
    log.info('ARGS: {0}'.format(ARGS))

    try:
        do_work()
    except Exception as error:
        log.warning('Inserter died: {0}'.format(error)) 

if __name__ == "__main__":
    '''
    This takes the command line args and passes them to the 'main' function
    '''
    PARSER = argparse.ArgumentParser(
        description='Run the DB inserter')
    PARSER.add_argument(
        '-f', '--folder', help='This is the folder to read.',
        default = None, required=False)
    PARSER.add_argument(
        '-ll', '--loglevel', default='INFO',
        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        help="Set log level for service (%s)" % 'INFO')
    ARGS = PARSER.parse_args()
    try:
        main(ARGS)
    except KeyboardInterrupt:
        log.warning('Keyboard Interrupt. Exiting...')
        # os._exit(0)
    except Exception as error:
        log.error('Other exception. Exiting with code 1...')
        log.error(error)
        # os._exit(1)