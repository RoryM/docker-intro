-----------------------------------------------------------------------
-- The user and DB is handled by the docker environment variables
--BEGIN;
--CREATE USER oceanmapper with encrypted password 'ocean_pw';
--COMMIT;


-----------------------------------------------------------------------

BEGIN;

-- geo schema will hold spatial data like eez, mpa etc
-- that gets loaded from shapefiles from the .sh script that gets called
-- after this one.
CREATE SCHEMA geo;
-- ais schema holds ais data in the same format as previously
CREATE SCHEMA ais;

--RAISE NOTICE 'Creating PostGIS Extenstion';
CREATE EXTENSION IF NOT EXISTS postgis; 

-- pos_reports holds AIS position reports
CREATE TABLE ais.pos_reports
(
    id text COLLATE pg_catalog."default" NOT NULL,
    event_time timestamp with time zone NOT NULL
);
 